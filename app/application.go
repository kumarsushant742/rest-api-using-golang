package app

import (
	"github.com/gin-gonic/gin"
)

var (
	router  = gin.Default()
)

func StarApplication() {
	handle.HandleRequests()
	router.Run(":8080")

}

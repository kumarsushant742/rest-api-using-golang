package app

import (
	"github.com/sushant/rest-api-using-golang/controller"
)

func HandleRequests() {
	router.GET("/ping", controller.Ping)
}

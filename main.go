package main

import (
	"database/sql"
	"fmt"
	"sushant/rest-api-using-golang/app"
	"sushant/rest-api-using-golang/handler"
	//"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "intel.123"
	dbname   = "postgres"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected!")

	app.StarApplication()

	// r := gin.Default()

	// r.GET("/books", controllers.FindBooks) // new
	// r.POST("/books", controllers.CreateBook)
	// r.GET("/books/:id", controllers.FindBook)
	// r.PATCH("/books/:id", controllers.UpdateBook)
	// r.DELETE("/books/:id")

	// r.Run()
}

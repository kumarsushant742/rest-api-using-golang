# This file is a template, and might need editing before it works on your project.
FROM golang:1.12.13

ENV REPO_URL=github.com/sushant/rest-api-using-golang

ENV GOPATH=/app

ENV APP_PATH=$GOPATH/src/REPO_URL

ENV WORKPATH=APP_PATH/src

COPY src $WORKPATH

WORKDIR $WORKPATH

RUN go build -o first-API .

EXPOSE 8081

CMD ["./first-API"]

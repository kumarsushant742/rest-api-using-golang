module sushant/rest-api-using-golang

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.8.0
	github.com/rahmanfadhil/gin-bookstore v0.0.0-20200805034825-de0f04215ba8
)
